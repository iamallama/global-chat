﻿using UnityEngine;
using System.Linq;
using Mirror;
using System.Text;
using System.Collections.Generic;

public partial class Chat {
    public enum GlobalChatError {
        None,
        CantFindPlayer,
        TooSoon,
        LevelTooLow,
        MissingItem
    }

    [Header("Global Chat")]
    public ChannelInfo global = new ChannelInfo("/a", "(global)", "(global)", new Color32(171, 235, 198, 255));

    [Tooltip("Optional. If the player has this item, they can use global chat. If this is null, then it is not required.")]
    public ScriptableItem globalChatItem;

    [Tooltip("Player must be at least this level to use the global chat.")]
    public int globalChatLevel = 1;

    [Tooltip("Number of seconds before the player can use global chat again.")]
    public float rateLimit = 1f;
    private float nextChatTime = 0f;

    public string errorTooSoon = "Must wait at least {RATELIMIT} seconds between messages.";
    public string errorLevelTooLow = "Must be at least level {MINLEVEL} before using global chat.";
    public string errorMissingItem = "You must have purchased {CHATITEM} before you can use global chat.";

    public void OnSubmit_GlobalChat(string text) {
        //if this is a global command
        if (text.StartsWith(global.command)) {
            //check for an error message with chat request
            GlobalChatError globalChatError = GlobalChatCheckForError();
            //and the user can use the global chat
            if (globalChatError == GlobalChatError.None) {
                //get the message part
                string content = ParseGeneral(global.command, text);

                //if message isn't empty
                if (!Utils.IsNullOrWhiteSpace(content)) {
                    //so far so good, sending to server
                    CmdMsgGlobalChat(content);

                    //set next time the player can chat
                    nextChatTime = Time.time + rateLimit;
                }
            } else {
                string errorMessage = "";

                switch (globalChatError) {
                    case GlobalChatError.LevelTooLow:
                        errorMessage = BuildErrorMessage(errorLevelTooLow);
                        break;
                    case GlobalChatError.MissingItem:
                        errorMessage = BuildErrorMessage(errorMissingItem);
                        break;
                    case GlobalChatError.TooSoon:
                        errorMessage = BuildErrorMessage(errorTooSoon);
                        break;
                }

                //if the error message is not empty
                if (!Utils.IsNullOrWhiteSpace(errorMessage)) {
                    chat.AddMessage(new ChatMessage("", info.identifierIn, errorMessage, "",  info.color));
                }
            }
        }
    }

    public GlobalChatError GlobalChatCheckForError() {
        Player player = GetComponent<Player>();

        //was the player found
        if(player != null) {
            //is the player at or above the required level
            if(player.level >= globalChatLevel) {
                //if enough time has passed since last message
                if(Time.time >= nextChatTime) {
                    //is the item null (not required)
                    if(globalChatItem == null){
                        return GlobalChatError.None;
                    }

					//is the item in the players inventory
					if (player.inventory.Any(i => i.amount > 0 && i.item.data == globalChatItem)) {
						return GlobalChatError.None;
					}
					
                    return GlobalChatError.MissingItem;
                } else {
                    return GlobalChatError.TooSoon;
                }
            } else {
                return GlobalChatError.LevelTooLow;
            }
        } else {
            return GlobalChatError.CantFindPlayer;
        }
    }

	[Command]
    void CmdMsgGlobalChat(string message) {
		if (Utils.IsNullOrWhiteSpace(message) || message.Length > maxLength || GlobalChatCheckForError() != GlobalChatError.None) return;

		// send message to all online guild members
		foreach (KeyValuePair<string, Player> pair in Player.onlinePlayers) {
			pair.Value.chat.TargetMsgGlobalChat(pair.Value.connectionToClient, name, message);
		}
	}

	[TargetRpc()] // only send to one client
    public void TargetMsgGlobalChat(NetworkConnection target, string sender, string message) {
        string reply = global.command + " " + sender + " ";
        chat.AddMessage(new ChatMessage(sender, global.identifierIn, message, reply, global.color));
    }

    public string BuildErrorMessage(string text) {
        StringBuilder errorText = new StringBuilder(text);

        errorText.Replace("{NAME}", name);
        errorText.Replace("{RATELIMIT}", rateLimit.ToString("0.#"));
        errorText.Replace("{MINLEVEL}", globalChatLevel.ToString());
        errorText.Replace("{CHATITEM}", globalChatItem.name);

        return errorText.ToString();
    }

}
