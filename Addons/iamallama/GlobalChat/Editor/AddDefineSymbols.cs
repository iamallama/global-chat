﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace llama.GlobalChat {
	/// <summary>
	/// Adds the given define symbols to PlayerSettings define symbols.
	/// Just add your own define symbols to the Symbols property at the below.
	/// </summary>
	[InitializeOnLoad]
	public class AddDefineSymbols : Editor {

		/// <summary>
		/// Symbols that will be removed from the editor
		/// </summary>
		public static readonly string[] SymbolsRemove = new string[] {
			 "IAMALLAMA_GLOBAL_CHAT"
		};

		/// <summary>
		/// Symbols that will be added to the editor
		/// </summary>
		public static readonly string[] Symbols = new string[] {
			 "IAMALLAMA_GLOBAL_CHAT"
		};

		/// <summary>
		/// Add define symbols as soon as Unity gets done compiling.
		/// </summary>
		static AddDefineSymbols() {
			string definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			List<string> allDefines = definesString.Split(';').ToList();
			allDefines.AddRange(Symbols.Except(allDefines));
			allDefines.RemoveAll(x => SymbolsRemove.Contains(x) && !Symbols.Contains(x));
			PlayerSettings.SetScriptingDefineSymbolsForGroup(
				EditorUserBuildSettings.selectedBuildTargetGroup,
				string.Join(";", allDefines.ToArray()));
		}

	}
}